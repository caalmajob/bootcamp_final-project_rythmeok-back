const express = require('express');
const path = require('path');
const passport = require('passport');
const cors = require('cors');
const session = require('express-session');
const MongoStore = require('connect-mongo')(session);
const mongoose = require('mongoose');
const indexRoutes = require('./routes/index.routes');
const concertRoutes = require('./routes/concert.routes.js')
const concertHallRoutes = require('./routes/concertHall.routes.js')
const chatRoutes = require('./routes/chat.routes.js')
const userRouter = require('./routes/user.routes');

require('dotenv').config();
const db = require('./db.js');

db.connect();

require('./passport/passport');

const PORT = process.env.PORT || 5000;

const app = express();

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
  res.header('Access-Control-Allow-Credentials', true);
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

app.use(cors({
  origin: ['http://localhost:3000'],
  credentials: true,
}));

app.use(express.json());
app.use(express.urlencoded({ extended: true}));

app.use(express.static(path.join(__dirname, 'public')));

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(
  session({
    secret: process.env.SESSION_SECRET,
    resave: false,
    saveUninitialized: false,
    cookie: {
      maxAge: 24 * 60 * 60 * 1000,
      httpOnly: false,
      secure: false,
      sameSite: false,
    },
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
  })
);

app.use(passport.initialize());
app.use(passport.session());

app.use('/', indexRoutes);
app.use("/auth", userRouter);
app.use('/concert', concertRoutes);
app.use('/concerthall', concertHallRoutes);
app.use('/chat', chatRoutes);

app.use('*', (req, res, next) => {
  const error = new Error('Route not found');
  error.status = 404;
  next(error);
})

app.use((err, req, res, next) => {
  return res.status(err.status || 500).json(err.message || 'Unexpected error');
});

app.listen(PORT, () => {
  console.log(`Server running in http://localhost:${PORT}`);
});