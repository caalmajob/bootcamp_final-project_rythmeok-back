const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const concertHallSchema = new Schema(
    {
        name: { type: String, required: true },
        address: { type: String, required: true },
        capacity: { type: Number },
        location: { type: String },
        logo: {type: String},
        concerts: [{ type: mongoose.Types.ObjectId, ref: 'Concert' }],
    },
    { timestamps: true },
);

const ConcertHall = mongoose.model('ConcertHall', concertHallSchema);

module.exports = ConcertHall;
