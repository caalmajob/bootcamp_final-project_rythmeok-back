const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const userSchema = new Schema({
    name:{type: String, required: true},
    email:{type: String, required: true},
    password: {type: String, required: true},
    rol:{type: String, enum:['basic', 'admin'], default: 'basic'},
    tickets:[{type: mongoose.Types.ObjectId, ref: 'Concert'}],
    friends: [{type: mongoose.Types.ObjectId, ref:'User'}],
    wishlist: [{type: mongoose.Types.ObjectId, ref:'Concert'}],
    chat: [{type: mongoose.Types.ObjectId, ref:'Chat'}],

}, {
    timestamps: true
})


const User = mongoose.model('User', userSchema);

module.exports = User;
