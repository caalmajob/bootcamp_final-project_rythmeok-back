const mongoose = require('mongoose');
const Schema = mongoose.Schema;


const concertSchema = new Schema({
    concertHall: {type: mongoose.Types.ObjectId, ref:'ConcertHall'},
    artist: {type: String, required: true},
    concertName: {type: String},
    date: {type: Date},
    price: {type: Number},
    attendees:[{type: mongoose.Types.ObjectId, ref: 'User'}],
    styles: [{ type: String}],
    likes: [{type: mongoose.Types.ObjectId, ref: 'User'}],
    location: {type: String},
    image: {type: String},
    detailConcert: {type: String},
    spotifyUrl: {type: String}
});

const Concert = mongoose.model('Concert', concertSchema);

module.exports = Concert
