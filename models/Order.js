const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const orderSchema = new Schema({
    user:{type: mongoose.Types.ObjectId, ref:'User'},
    concert: {type: mongoose.Types.ObjectId, ref:'Concert'},
    quantity: {type: Number},
    totalPrice: {type: Number},
},{timestamps: true})


const Order = mongoose.model('Order', orderSchema);

module.exports = Order;