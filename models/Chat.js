const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const chatSchema = new Schema({
    user: {type: mongoose.Types.ObjectId, ref: 'User'},
    messages:{type: Array}
},{timestamps: true})

const Chat = mongoose.model('Chat', chatSchema);

module.exports = Chat;