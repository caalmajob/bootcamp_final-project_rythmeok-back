const express = require('express');
const chatCtrl = require('../controllers/chat.controller');

const router = express.Router();

router.get('/', chatCtrl.allChatsGet);
 
router.post('/add', chatCtrl.messagePut)

module.exports = router;
