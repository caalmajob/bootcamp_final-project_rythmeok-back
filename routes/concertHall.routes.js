const express = require('express');
const router = express.Router();

const concertHallCtrl = require('../controllers/concertHall.controller')

router.get('/', concertHallCtrl.allConcertHallGet);

module.exports = router;
