const express = require('express');
const router = express.Router();
const concertCtrl = require('../controllers/concert.controller');

router.get('/', concertCtrl.allConcertsGet);
router.post('/buyconcert', concertCtrl.buyConcertPut);
router.put('/likes', concertCtrl.handleLikePut);
router.post('/wishlist', concertCtrl.handleWishlistPut);

module.exports = router;
