const mongoose = require('mongoose');

// const DB_URL = 'mongodb+srv://db_rythmeok:upgradehub21@cluster0.wqjgv.mongodb.net/rythmeok';
// const DB_URL = 'mongodb://localhost:27017/rythmeok';
const DB_URL = process.env.DB_URL;


const connect = async () => {
    try {
        await mongoose.connect(DB_URL, {
            useNewUrlParser: true,
            useUnifiedTopology: true,
            useFindAndModify: false,
        });
        console.log('Conectado a la DB')
    } catch(error) {
        console.log('Error conectando con la base de datos', error)
    }
}

module.exports = {connect: connect, DB_URL};
