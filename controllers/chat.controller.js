const Chat = require('../models/Chat');

    const allChatsGet = async (req, res, next) => {
        try {
        const chat = await Chat.find()
        .populate('user');
        return res.json(chat);
        } catch(error) {
            next(error);
        }
    }


    const messagePut = async (req, res, next) => {
        try {
            const { id, message } = req.body;
    
            const newMessage = await Chat.findByIdAndUpdate(
                id,
                { $push: { messages: message } },
                { new: true }
            );
            const chat = await Chat.find()
            .populate('user');
            return res.json(chat);
    
        } catch(error) {
            next(error);
        }
    };

module.exports = {
    allChatsGet,
    messagePut
}