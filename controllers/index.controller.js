const renderIndex = (req, res, next) => {
    return res.render('index', { title: 'Main Page', user: req.user});
}

module.exports = {
    renderIndex
}