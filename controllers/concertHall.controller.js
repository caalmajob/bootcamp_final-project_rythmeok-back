const ConcertHall = require('../models/ConcertHall');

const allConcertHallGet = async (req, res, next) => {
    try {
    const concertHall = await ConcertHall.find()
    .populate('concerts');
    return res.json(concertHall);
    } catch(error) {
        next(error);
    }
}

module.exports = {
    allConcertHallGet
}