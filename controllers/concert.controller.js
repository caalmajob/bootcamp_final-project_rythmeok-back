const Concert = require('../models/Concert');
const User = require('../models/User');

const allConcertsGet = async (req, res, next) => {
    try {
        const concert = await Concert.find()
            .populate({
                path: 'tickets',
                populate: {
                    path: 'concertHall',
                    model: 'ConcertHall'
                }
            })
            .populate('attendees')
            .populate('concertHall')
            .populate('likes');

        return res.json(concert);
    } catch (error) {
        next(error);
    }
};

const buyConcertPut = async (req, res, next) => {
    try {
        const { id, userId } = req.body;

        const boughtConcert = 
        await Concert.findByIdAndUpdate(
            id,
            {
                $push: { attendees: userId }
            },
            { new: true }
        );
        await User.findByIdAndUpdate(
            userId,
            {
                $push: { tickets: id }
            },
            { new: true }
        );
        return res.status(200).json(boughtConcert);

    } catch (error) {
        next(error);
    }
};

const handleLikePut = async (req, res, next) => {
    try {
        const userId = req.user._id;
        const { id } = req.body;

        const concert = await Concert.findById(id);
        if (concert.likes.includes(userId)) {
            const index = concert.likes.indexOf(userId);
            concert.likes.splice(index, 1);
            const likeConcert = await Concert.findByIdAndUpdate(
                id,
                { likes: concert.likes },
                { new: true }
            );
            return res.status(200).json(likeConcert);

        } else {
            const likeConcert = await Concert.findByIdAndUpdate(
                id,
                {
                    $push: { likes: userId }
                },
                { new: true }
            );
            return res.status(200).json(likeConcert);
        }

    } catch (error) {
        next(error);
    }
};

const handleWishlistPut = async (req, res, next) => {
    try {
        const { id, userId } = req.body;

        const user = await User.findById(userId);
        if (user.wishlist.includes(id)) {
            const index = user.wishlist.indexOf(id);
            user.wishlist.splice(index, 1);
            const wishlistItem = await User.findByIdAndUpdate(
                userId,
                { wishlist: user.wishlist },
                { new: true }
            );
            return res.status(200).json(wishlistItem);

        } else {
            const wishlistItem = await User.findByIdAndUpdate(
                userId,
                {
                    $push: { wishlist: id }
                },
                { new: true }
            );
            return res.status(200).json(wishlistItem);
        }

    } catch (error) {
        next(error);
    }
};

module.exports = {
    allConcertsGet,
    buyConcertPut,
    handleLikePut,
    handleWishlistPut,
}