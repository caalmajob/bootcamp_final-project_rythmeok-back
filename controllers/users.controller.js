const passport = require("passport");
const User = require('../models/User');

const getUser = async (user) => {
    const userData = await User.findById(user._id)
      .populate({path: 'tickets',
        populate: {path: 'concertHall', model: 'ConcertHall'}})
      .populate('friends')
      .populate('wishlist');
    userData.password = null;
    return userData;
}

module.exports = {

  checkSession: async (req, res, next) => {
      if(req.user) {
        const userData = await getUser(req.user);

        return res.status(200).json(userData);
      } else {
        return res.status(401).json({message: 'No user found'});
      }
  },

  registerPost: (req, res, next) => {
    const { password, email, name } = req.body;

    if(!password || !email || !name) {
      return res.status(400).json({ message: 'Completa todos los campos' });
    }

    passport.authenticate("register", (error, user) => {
      if (error) {
        return res.status(403).json({message: error.message});
      }

      req.logIn(user, (error) => {
        if (error) {
          return res.status(403).json({message: error.message});
        };

        let userRegister = user;
        userRegister.password = null;

        return res.json(userRegister);
      });
    })(req, res, next);
  },

  loginPost: (req, res, next) => {
    passport.authenticate("login", (error, user) => {
      if (error) {
        return res.json({message: error.message});
      }

      req.logIn(user, async (error) => {
        if (error) {
          return res.json({message: error.message});
        };

        const userData = await getUser(user);

        return res.json(userData);
      });
    })(req, res, next);
  },

  logoutPost: (req, res, next) => {
    if (req.user) {
      req.logout();

      req.session.destroy(() => {
        res.clearCookie("connect.sid");

        return res.status(200).json({ message: 'Logout successful' });
      });
    } else {
      return res.status(401).json({ message: 'Unexpected error' });
    }
  },
};