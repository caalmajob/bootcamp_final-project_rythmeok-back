require('dotenv').config();
const mongoose = require('mongoose');
const db = require('../db');
const User = require('../models/User.js');

const users = [
    {
        email: "Oscar@gmail.com",
        password: "12345asd",
        name: 'Oscar Sanz',
        rol: 'admin',
        friends: [],
        whislist: [],
    },
    {
        email: "laura@gmail.com",
        password: "12345asd",
        name: 'Laura García',
        rol: 'admin',
        friends: [],
        whislist: [],
    },
    {
        email: "caalmajob@gmail.com",
        password: "12345asd",
        name: 'Carlos Alcalá',
        rol: 'admin',
        friends: [],
        whislist: [],
    
    }
];

mongoose
    .connect(db.DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
        console.log('Deleting all users...');
        const allUsers = await User.find();


        if (allUsers.length) {
            await User.collection.drop();
        }
    })
    .catch(error => {
        console.log('Error deleting data: ', error)
    })
    .then(async () => {
        await User.insertMany(users);
        console.log('Successfully added users to DB...');
    })
    .finally(() => mongoose.disconnect());
