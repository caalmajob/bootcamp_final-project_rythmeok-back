require('dotenv').config();
const mongoose = require('mongoose');
const db = require('../db');
const ConcertHall = require('../models/ConcertHall');
const User = require('../models/User.js');

const concertHalls = [
    {
        _id: '60abbbedd10a1c20314427cc',
        name: 'SALA CARACOL',
        address: 'C. de Bernardino Obregón, 18, 28012 Madrid',
        capacity: 300,
        location: 'MADRID - ESPAÑA',
        logo: 'https://www.salacaracol.com/wp-content/uploads/2018/05/01-4.jpg',
        concerts: ['60abbd2052d4212158049c26', '60abbd2052d4212158049c27', '60abbd2052d4212158049c28', '60abba96b888101f068293ef'],
    },
    {
        _id: '60abbbedd10a1c20314427cd',
        name: 'GRUTA 77',
        address: 'Calle Cuclillo, 6, 28019 Madrid',
        capacity: 200,
        location: 'MADRID - ESPAÑA',
        logo: 'https://www.manerasdevivir.com/pics/noticias/2015/gruta-77-rel.jpg',
        concerts: ['60abba96b888101f068293f0', '60abba96b888101f068293f1', '60abba96b888101f068293f2'],
    },
    {
        _id: '60abbbedd10a1c20314427ce',
        name: 'SALA CLAMORES',
        address: 'Calle de Alburquerque, 14, 28010 Madrid',
        capacity: 500,
        location: 'MADRID - ESPAÑA',
        logo: 'https://lanocheenvivo.com/Multimedia/Salas/111/19092016130534_Captura%20de%20pantalla%202016-09-19%20a%20las%2013.04.28.png',
        concerts: ['60abbd2052d4212158049c26', '60abbd2052d4212158049c27', '60abbd2052d4212158049c28', '60abba96b888101f068293ef'],
    },
    {
        _id: '60abbbedd10a1c20314427cf',
        name: 'WURLITZER BALLROOM',
        address: 'Calle de las Tres Cruces, 12, 28013 Madrid',
        capacity: 150,
        location: 'MADRID - ESPAÑA',
        logo: 'https://s3.eu-central-1.amazonaws.com/images.jacksonlive.es/upload/spots/high/2041585608456.jpg',
        concerts: ['60abba96b888101f068293f0', '60abba96b888101f068293f1', '60abba96b888101f068293f2'],
    },
    {
        _id: '60abbbedd10a1c20314427d0',
        name: 'SALA LA RIVIERA',
        address: 'Paseo Bajo de la Virgen del Puerto, S/N, 28005 Madrid',
        capacity: 600,
        location: 'MADRID - ESPAÑA',
        logo: 'https://s3.eu-central-1.amazonaws.com/images.jacksonlive.es/upload/spots/high/1141585608411.jpg',
        concerts: ['60abbd2052d4212158049c26', '60abbd2052d4212158049c27', '60abbd2052d4212158049c28', '60abba96b888101f068293ef'],
    },
    {
        _id: '60abbbedd10a1c20314427d1',
        name: 'RAZZMATAZZ',
        address: 'Carrer dels Almogàvers, 122, 08018 Barcelona',
        capacity: 800,
        location: 'BARCELONA - ESPAÑA',
        logo: 'https://s3.eu-central-1.amazonaws.com/images.jacksonlive.es/upload/spots/4x3/561587497258.jpg',
        concerts: ['60abba96b888101f068293f3', '60abba96b888101f068293f4', '60abba96b888101f068293f5', '60abba96b888101f068293f6'],
    },
    {
        _id: '60abbbedd10a1c20314427d2',
        name: 'SALA BÓVEDA',
        address: 'Carrer de Roc Boronat, 33, 08005 Barcelona',
        capacity: 300,
        location: 'BARCELONA - ESPAÑA',
        logo: 'https://s3.eu-central-1.amazonaws.com/images.jacksonlive.es/upload/spots/high/651585608387.jpg',
        concerts: ['60abba96b888101f068293f3', '60abba96b888101f068293f4', '60abba96b888101f068293f5', '60abba96b888101f068293f6'],
    },
    {
        _id: '60abbbedd10a1c20314427d3',
        name: 'SALA APOLO',
        address: 'Carrer Nou de la Rambla, 113, 08004 Barcelona',
        capacity: 450,
        location: 'BARCELONA - ESPAÑA',
        logo: 'https://s3.eu-central-1.amazonaws.com/images.jacksonlive.es/upload/spots/high/231587245728.jpg',
        concerts: ['60abba96b888101f068293f3', '60abba96b888101f068293f4', '60abba96b888101f068293f5', '60abba96b888101f068293f6'],
    },
];

mongoose
    .connect(db.DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
        console.log('Deleting all concertHalls...');
        const allConcertHalls = await ConcertHall.find();

        if(allConcertHalls.length) {
            await ConcertHall.collection.drop();
        }
    })
    .catch(error => {
        console.log('Error deleting concertHall data: ', error)
    })
    .then(async () => {
        await ConcertHall.insertMany(concertHalls);
        console.log('Succesfully added concertHalls to DB...');
    })
    .finally(() => mongoose.disconnect());