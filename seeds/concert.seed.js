require('dotenv').config();
const mongoose = require('mongoose');
const db = require('../db');
const Concert = require('../models/Concert');
const User = require('../models/User.js');

const concerts = [
    {
        _id: '60abbd2052d4212158049c26',
        concertHall: '60abbbedd10a1c20314427cc',
        artist: 'Dua Lipa',
        concertName: 'Dua Lipa - Madrid 2021',
        date: '2021-10-17',
        price: 50,
        styles: ['Pop', 'Disco', 'House', 'R&B'],
        location: 'MADRID - ESPAÑA',
        image: 'https://cloudfront-us-east-1.images.arcpublishing.com/eluniverso/EH64VO6YXBGCVOX4ZTBS2CMAB4.jpg',
        detailConcert: 'Dua Lipa (Londres, 22 de agosto de 1995) es una cantante y compositora británica de etnia albanokosovar. Después de trabajar como modelo, firmó con Warner Bros. Records en 2014 y lanzó su álbum debut homónimo en 2017. El álbum alcanzó el número 3 en la UK Albums Chart y lanzó ocho sencillos, incluidos «Be the One» e «IDGAF», y el sencillo número 1 del Reino Unido «New Rules», que también llegó al puesto número 6 en Estados Unidos. En 2018, ganó dos premios Brit a la Mejor artista solista femenina británica y al Artista revelación británico.',
        spotifyUrl: 'https://open.spotify.com/embed/artist/6M2wZ9GZgrQXHCFfjv46we',
    },
    {
        _id: '60abbd2052d4212158049c27',
        concertHall: '60abbbedd10a1c20314427cd',
        artist: 'Danny Ocean',
        concertName: 'Danny Ocean Live',
        date: '2021-06-17',
        price: 36,
        styles: ['Pop latino', 'Dancehall', 'Moombahton', 'Reguetón'],
        location: 'MADRID - ESPAÑA',
        image: 'https://i2.wp.com/plus.cusica.com/wp-content/uploads/2019/07/Danny-Ocean.jpg?fit=1548%2C1024&ssl=1',
        detailConcert: "Daniel Alejandro Morales Reyes (Caracas, Venezuela; 5 de mayo de 1992),1​ más conocido como Danny Ocean,2​ es un cantante, diseñador gráfico y productor venezolano,3​ de música latina, reguetón, dancehall y moombahton,4​ nominado a múltiples premios, reconocido principalmente por su tema «Me rehúso», lanzado inicialmente de manera independiente en 2016 a través de su canal de YouTube y viralizado por Shazam y Spotify.",
        spotifyUrl: 'https://open.spotify.com/embed/artist/5H1nN1SzW0qNeUEZvuXjAj',
    },
    {
        _id: '60abbd2052d4212158049c28',
        concertHall: '60abbbedd10a1c20314427cd',
        artist: 'KISS',
        concertName: 'Kiss - Paquetes VIP',
        date: '2021-07-04',
        price: 80,
        styles: ['Hard rock', 'Heavy metal', 'Glam metal', 'Shock rock'],
        location: 'MADRID - ESPAÑA',
        image: 'https://myfest.es/wp-content/uploads/myfest-kiss.jpg',
        detailConcert: "Kiss (estilizado KISS) es una banda estadounidense de rock formada en Nueva York en enero de 1973 por el bajista Gene Simmons y el guitarrista Paul Stanley, a los que más tarde se unirían el batería Peter Criss y el guitarrista Ace Frehley.2​ Conocidos por su maquillaje facial y sus extravagantes trajes, el grupo se dio a conocer al público a mediados de los años 1970 gracias a sus actuaciones en directo, en las que incluían pirotecnia, llamaradas, cañones, baterías elevadoras, guitarras con humo y sangre falsa. Si se tienen en cuenta los álbumes en solitario de 1978, Kiss ha conseguido treinta discos de oro de la RIAA hacia 2015 y se la considera la banda estadounidense que ha recibido más de estas certificaciones.",
        spotifyUrl: 'https://open.spotify.com/embed/artist/07XSN3sPlIlB2L2XNcTwJw',
    },
    {
        _id: '60abba96b888101f068293ef',
        concertHall: '60abbbedd10a1c20314427ce',
        artist: 'Limp Bizkit',
        concertName: 'Limp Bizkit',
        date: '2021-08-25',
        price: 25,
        styles: ['Rap metal', 'Rapcore'],
        location: 'MADRID - ESPAÑA',
        image: 'https://cdn.wegow.com/media/artist-media/limp-bizkit/limp-bizkit-1505817247.-1x1780.jpg',
        detailConcert: "Limp Bizkit es una banda estadounidense de Rapcore y rap metal formada en la ciudad de Jacksonville, Florida, en 1994. Sus creadores fueron el líder y vocalista Fred Durst y el bajista Sam Rivers. Posteriormente se unirían el primo de Rivers John Otto como baterista, y el guitarrista Rob Waters, sustituido más tarde por Wes Borland. Un tiempo después se unió el exmiembro de House of Pain DJ Lethal, completando la formación. La banda ha vendido hasta la fecha más de 40 millones de copias en todo el mundo.",
        spotifyUrl: 'https://open.spotify.com/embed/artist/165ZgPlLkK7bf5bDoFc6Sb',
    },
    {
        _id: '60abba96b888101f068293f0',
        concertHall: '60abbbedd10a1c20314427ce',
        artist: 'Tash Sultana',
        concertName: 'Tash Sultana & Milky Chance',
        date: '2021-09-07',
        price: 35,
        styles: ['Rock Psicodélico'],
        location: 'MADRID - ESPAÑA',
        image: 'https://files2.soniccdn.com/images/articles/original/9234.jpg',
        detailConcert: "Nació y creció en Melbourne, Australia el 15 de junio de 1995, es de ascendencia maltesa, Empezó su afición a la música tocando, a los tres años, la guitarra que le regaló su abuelo. actualmente puede tocar 20 instrumentos entre ellos la guitarra, bajo, saxofón, flauta, trompeta y percusión. Se dio a conocer en las calles de su ciudad con una guitarra eléctrica, un juego de amplificadores, cajas de efectos y loops transportados en un diablito. Lamentablemente, a los 17 años, Sultana paso por un periodo de exploración y crisis existencial, lo que la llevo a insertarse en un medio de abuso de drogas desarrollando un trastorno psicótico consumió variedad de drogas ilicitas, excepto la heroína. La última vez, tuvo una pésima experiencia con hongos alucinógenos. Puso bastantes hongos en un pedazo de pizza y tardó nueve meses en estabilizarse y desintoxicarse por completo. Encontró la musicoterapia y descubrió que la respuesta había estado siempre ahí: la música era su pasión. De tal manera que consiguió alejar el dolor y crear música.",
        spotifyUrl: 'https://open.spotify.com/embed/artist/6zVFRTB0Y1whWyH7ZNmywf',
    },
    {
        _id: '60abba96b888101f068293f1',
        concertHall: '60abbbedd10a1c20314427cf',
        artist: 'Arch Enemy',
        concertName: 'Arch Enemy - Behemoth',
        date: '2021-10-09',
        price: 40,
        styles: ['Death Metal Melódico'],
        location: 'MADRID - ESPAÑA',
        image: 'https://www.todorock.com/wp-content/uploads/2018/10/arch_enemy-1200x900.jpg',
        detailConcert: "En sus inicios exploró el death metal original, pero sufrió una transformación musical después del cambio de integrantes que tuvo, y comenzó a hacer un death metal más melódico, que sigue haciendo actualmente. Sus letras hablan de rebelión y suelen criticar a la sociedad y a la religión cristiana4​ La banda es procedente de Suecia, formada en 1996 por el guitarrista Michael Amott5​ junto al vocalista Johan Liiva. La banda ha publicado diez álbumes de estudio, dos en directo, tres EP y dos DVD. La banda fue originalmente liderada por Johan Liiva, hasta que Angela Gossow se unió a la banda como vocalista en 2001.6​7​ En 2014, Gossow dejó la banda y fue remplazada por Alissa White-Gluz, exvocalista de The Agonist.",
        spotifyUrl: 'https://open.spotify.com/embed/artist/0DCw6lHkzh9t7f8Hb4Z0Sx',
    },
    {
        _id: '60abba96b888101f068293f2',
        concertHall: '60abbbedd10a1c20314427d0',
        artist: 'Bon Iver',
        concertName: 'Bon Iver - I, I',
        date: '2021-11-16',
        price: 25,
        styles: ['Post rock', 'Folk'],
        location: 'MADRID - ESPAÑA',
        image: 'https://www.mondosonoro.com/wp-content/uploads/2016/08/boniver16.jpg',
        detailConcert: "Bon Iver es una banda de indie folk estadounidense fundada en 2006 por el cantautor Justin Vernon. Otros componentes de su banda son Mike Noyce, Sean Carey, Colin Stetson y Matthew McCaughan, entre otros esporádicos. Vernon produjo de forma independiente su primer álbum, For Emma, Forever Ago en 2007, grabándolo en su mayor parte mientras pasaba tres meses en una cabaña remota en Wisconsin.3​En 2012, la banda ganó el Grammy al Mejor Nuevo Artista y al Mejor Álbum de Música Alternativa por su álbum Bon Iver, Bon Iver; además de estar nominados a Canción y Grabación del Año por 'Holocene', incluida en ese mismo álbum. Su nombre, Bon Iver, es un juego de palabras a partir del francés 'bon hiver' (buen invierno, pronunciación en francés: /bɔn‿ivɛːʁ/), tomado de un saludo de la serie 'Doctor en Alaska' (Northern Exposure).",
        spotifyUrl: 'https://open.spotify.com/embed/artist/4LEiUm1SRbFMgfqnQTwUbQ',
    },
    {
        _id: '60abba96b888101f068293f3',
        concertHall: '60abbbedd10a1c20314427d1',
        artist: 'Dua Lipa',
        concertName: 'Dua Lipa - Future Nostalgia',
        date: '2021-10-15',
        price: 53,
        styles: ['Pop', 'Disco', 'House', 'R&B'],
        location: 'BARCELONA - ESPAÑA',
        image: 'https://www.totalisimo.com/wp-content/uploads/2018/12/Dua-Lipa.portada.jpg',
        detailConcert: 'Dua Lipa (Londres, 22 de agosto de 1995) es una cantante y compositora británica de etnia albanokosovar. Después de trabajar como modelo, firmó con Warner Bros. Records en 2014 y lanzó su álbum debut homónimo en 2017. El álbum alcanzó el número 3 en la UK Albums Chart y lanzó ocho sencillos, incluidos «Be the One» e «IDGAF», y el sencillo número 1 del Reino Unido «New Rules», que también llegó al puesto número 6 en Estados Unidos. En 2018, ganó dos premios Brit a la Mejor artista solista femenina británica y al Artista revelación británico.',
        spotifyUrl: 'https://open.spotify.com/embed/artist/6M2wZ9GZgrQXHCFfjv46we',
    },
    {
        _id: '60abba96b888101f068293f4',
        concertHall: '60abbbedd10a1c20314427d2',
        artist: 'Danny Ocean',
        concertName: 'Danny Ocean Bcn',
        date: '2021-06-18',
        price: 36,
        styles: ['Pop latino', 'Dancehall', 'Moombahton', 'Reguetón'],
        location: 'BARCELONA - ESPAÑA',
        image: 'https://wal.group/images/posts/como-me-rehuso-llevo-a-danny-ocean-a-la-cuspide-de-la-musica.jpg',
        detailConcert: "Daniel Alejandro Morales Reyes (Caracas, Venezuela; 5 de mayo de 1992),1​ más conocido como Danny Ocean,2​ es un cantante, diseñador gráfico y productor venezolano,3​ de música latina, reguetón, dancehall y moombahton,4​ nominado a múltiples premios, reconocido principalmente por su tema «Me rehúso», lanzado inicialmente de manera independiente en 2016 a través de su canal de YouTube y viralizado por Shazam y Spotify.",
        spotifyUrl: 'https://open.spotify.com/embed/artist/5H1nN1SzW0qNeUEZvuXjAj',
    },
    {
        _id: '60abba96b888101f068293f5',
        concertHall: '60abbbedd10a1c20314427d3',
        artist: 'Morat',
        concertName: 'Morat - ¿A dónde vamos?',
        date: '2021-09-09',
        price: 25,
        styles: ['Folk Pop', 'Pop'],
        location: 'BARCELONA - ESPAÑA',
        image: 'https://cdn.forbes.co/2020/10/Morat-1280x720-1.jpg',
        detailConcert: "Morat es una agrupación musical de folk pop de Bogotá, Colombia, creada en 2015. Sus integrantes son Juan Pablo Isaza Piñeros, Juan Pablo Villamil Cortés, Simón Vargas Morales y Martín Vargas Morales. Anteriormente, hasta finales de 2016 había formado parte de la banda Alejandro Posada, pero él decidió dejar el grupo por lo que fue sustituido por Martín Vargas Morales (hermano de Simón Vargas Morales). Hasta ahora han tenido mucho éxito y son de los grupos más amados por sus fans.",
        spotifyUrl: 'https://open.spotify.com/embed/artist/5C4PDR4LnhZTbVnKWXuDKD',
    },
    {
        _id: '60abba96b888101f068293f6',
        concertHall: '60abbbedd10a1c20314427d3',
        artist: 'Haim',
        concertName: 'Haim',
        date: '2021-07-08',
        price: 46,
        styles: ['Indie rock',
            'Indie pop',
            'Pop rock',
            'Soft rock',
            'R&B contemporáneo',
            'Rhythm and blues alternativo'],
        location: 'BARCELONA - ESPAÑA',
        image: 'https://cdn.wegow.com/media/artist-media/haim/haim-1515432819.-1x1780.jpg',
        detailConcert: "Haim es una banda estadounidense formada originalmente en 2006 en el Valle de San Fernando, Los Ángeles, California.1​2​ Su primer concierto fue el 7 de julio de 2007, pero no fue sino hasta el 2012 que comenzaron a publicar sus propias composiciones. La banda está formada por las hermanas Este Haim (14 de marzo de 1986), Danielle Haim (16 de febrero de 1989) y Alana Haim (15 de diciembre de 1991) y Fer Alvarez (03 de septiembre de 1993), acompañadas por el baterista Dash Hutton, que anteriormente fue miembro de bandas oriundas de Los Ángeles como Wires on Fire y Slang Chicken.​ Habitualmente son comparadas con Fleetwood Mac.",
        spotifyUrl: 'https://open.spotify.com/embed/artist/4Ui2kfOqGujY81UcPrb5KE',
    },
];

mongoose
    .connect(db.DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
        console.log('Deleting all concerts...');
        const allConcerts = await Concert.find();

        if(allConcerts.length) {
            await Concert.collection.drop();
        }
    })
    .catch(error => {
        console.log('Error deleting concert data: ', error)
    })
    .then(async () => {
        await Concert.insertMany(concerts);
        console.log('Succesfully added concerts to DB...');
    }).
    finally(() => mongoose.disconnect());