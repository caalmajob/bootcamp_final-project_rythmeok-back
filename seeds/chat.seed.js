require('dotenv').config();
const mongoose = require('mongoose');
const db = require('../db');
const Chat = require('../models/Chat');

const chats = [
    {
        user: "60b0c881c21dee2a356b19e2",
        messages: ['Bienvenido al chat de grupo', 'Entabla amistad con otros usuarios y comparte gustos musicales'],
    },
    {
        user: "60b10947b070f145947fce7b",
        messages: ['Hola soy el administrador', 'Si tienes algun problema solo tienes que consultarme'],
    },
];

mongoose
    .connect(db.DB_URL, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
    })
    .then(async () => {
        console.log('Deleting all messages...');
        const allChat = await Chat.find();


        if (allChat.length) {
            await Chat.collection.drop();
        }
    })
    .catch(error => {
        console.log('Error deleting data: ', error)
    })
    .then(async () => {
        await Chat.insertMany(chats);
        console.log('Successfully added messages to DB...');
    })
    .finally(() => mongoose.disconnect());
